<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'System Admin',
            'guard_name' => 'api',
        ]);
        $permissions = [
            'view-users', 'manage-users',
            'view-roles', 'manage-roles',
            'view-permissions', 'manage-permissions'
        ];
        $allperms = collect([]);
        foreach ($permissions as $permission) {
            $perm = Permission::create([
                'name' => $permission,
                'guard_name' => 'user-api',
            ]);
            $allperms->push($perm->id);
        }
        $role->sync($allperms->toArray());
    }
}
